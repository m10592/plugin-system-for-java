package pluginloader.load;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ParseResult;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.Modifier;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.PackageDeclaration;
import com.github.javaparser.ast.body.TypeDeclaration;
import pluginloader.ClassInfo;
import pluginloader.PluginLoader;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class SourceCodeParser {
    public ClassInfo parseClassInfo(String plugin) throws PluginLoader.LoadingException {
        JavaParser javaParser = new JavaParser();
        ParseResult<CompilationUnit> parseResult = javaParser.parse(plugin);
        if (!parseResult.isSuccessful()){
            StringBuilder message = new StringBuilder();
            message.append("Parsing source file failed:\n");
            parseResult.getProblems().forEach(problem -> message.append(String.format("%s\n", problem.getVerboseMessage())));
            throw new PluginLoader.LoadingException(message.toString());
        }
        assert parseResult.getResult().isPresent();
        final Set<TypeDeclaration<?>> publicTopLevelClasses = parseResult.getResult().get().getTypes().stream()
                .filter(TypeDeclaration::isTopLevelType)
                .filter(typeDeclaration -> !typeDeclaration.getModifiers().contains(Modifier.privateModifier()))
                .filter(typeDeclaration -> !typeDeclaration.getModifiers().contains(Modifier.protectedModifier()))
                .collect(Collectors.toSet());
        if (publicTopLevelClasses.isEmpty()){
            throw new PluginLoader.LoadingException("Did not found a public top level class in the source code");
        }
        if (publicTopLevelClasses.size() > 1){
            throw new PluginLoader.LoadingException("Found multiple public top level classes in the source code");
        }
        String className = publicTopLevelClasses.iterator().next().getName().asString();

        String packageName = null;
        Optional<Node> parentNode = publicTopLevelClasses.iterator().next().getParentNode();
        assert parentNode.isPresent();
        if ((parentNode.get() instanceof CompilationUnit)){
            CompilationUnit compilationUnit = (CompilationUnit) parentNode.get();
            Optional<PackageDeclaration> packageDeclaration = compilationUnit.getPackageDeclaration();
            if (packageDeclaration.isPresent()){
                packageName = packageDeclaration.get().getNameAsString();
            }
        }
        return new ClassInfo(className, packageName);
    }
}
