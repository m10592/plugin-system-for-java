package pluginloader.load;

import java.util.HashMap;
import java.util.Map;

public class PluginClassLoader extends ClassLoader{
    private static final Map<String, byte[]> classes = new HashMap<>();

    public PluginClassLoader() {
        super(PluginClassLoader.class.getClassLoader());
    }

    @Override
    public Class<?> findClass(String fullQualifiedClassName) throws ClassNotFoundException {
        if (!classes.containsKey(fullQualifiedClassName)){
            throw new ClassNotFoundException(String.format("Did not find class '%s'", fullQualifiedClassName));
        }
        Class<?> compiledClass = defineClass(fullQualifiedClassName, classes.get(fullQualifiedClassName), 0, classes.get(fullQualifiedClassName).length);
        return compiledClass;
    }

    @Override
    public Class<?> loadClass(String fullQualifiedClassName) throws ClassNotFoundException {
        if (classes.containsKey(fullQualifiedClassName)){
            return findClass(fullQualifiedClassName);
        }
        ClassLoader defaultLoader = Thread.currentThread().getContextClassLoader();
        Class<?> loadedClass = defaultLoader.loadClass(fullQualifiedClassName);
        return loadedClass;
    }

    public void putClassCode(String fullQualifiedClassName, byte[] compiledClassCode){
        assert fullQualifiedClassName != null;
        assert compiledClassCode != null;
        classes.put(fullQualifiedClassName, compiledClassCode);
    }
}
