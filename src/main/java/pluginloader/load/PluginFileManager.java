package pluginloader.load;

import javax.tools.*;
import java.io.IOException;

public class PluginFileManager extends ForwardingJavaFileManager<StandardJavaFileManager> {
    private final JavaFileObject javaFileObject;

    public PluginFileManager(StandardJavaFileManager fileManager, JavaFileObject javaFileObject) {
        super(fileManager);
        this.javaFileObject = javaFileObject;
    }

    @Override
    public JavaFileObject getJavaFileForOutput(Location location, String className, JavaFileObject.Kind kind, FileObject sibling) throws IOException {
        return javaFileObject;
    }
}
