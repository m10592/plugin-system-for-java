package pluginloader;

public class ClassInfo {
    private final String className;
    private final String packageName;

    public ClassInfo(String className, String packageName) {
        this.className = className;
        this.packageName = packageName;
    }

    public String getFullQualifiedClassName(){
        String packagePath = "";
        if (packageName != null){
            packagePath = String.format("%s.", packageName);
        }
        return String.format("%s%s", packagePath, className);
    }
}
