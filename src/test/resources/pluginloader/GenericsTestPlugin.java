package pluginloader;

import pluginloader.PluginLoaderTest;

import java.util.Arrays;
import java.util.List;

public class GenericsTestPlugin implements PluginLoaderTest.TestInterface {
    @Override
    public boolean successfulLoaded() {
        List<Boolean> testCodes = Arrays.asList(
                true,
                false,
                true
        );
        return testCodes.get(0);
    }
}