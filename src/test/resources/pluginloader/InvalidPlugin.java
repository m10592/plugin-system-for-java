package pluginloader;

import pluginloader.PluginLoaderTest;

public class InvalidPlugin implements PluginLoaderTest.TestInterface {
    private boolean success = true;

    public InvalidPlugin(boolean success) {
        this.success = success;
    }

    @Override
    public boolean successfulLoaded() {
        return success;
    }
}