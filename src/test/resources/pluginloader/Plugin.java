package pluginloader;

import pluginloader.PluginLoaderTest;

public class Plugin implements PluginLoaderTest.TestInterface {
    private boolean success = true;

    @Override
    public boolean successfulLoaded() {
        return success;
    }
}