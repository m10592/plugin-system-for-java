package pluginloader;

import collector.plugin.CollectorPlugin;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.jar.JarInputStream;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

public class PluginLoaderTest {
   public interface TestInterface{
       boolean successfulLoaded();
   }

   public interface LiveUpdateTestInterface {
       boolean updated();
   }

    @Test
    void load() throws PluginLoader.LoadingException {
        InputStream stream = ClassLoader.getSystemResourceAsStream("pluginloader/Plugin.java");
        assert stream != null;
        String testScraper = new BufferedReader(new InputStreamReader(stream)).lines().collect(Collectors.joining(System.lineSeparator()));
        PluginLoader pluginLoader = new PluginLoader();
        TestInterface loadedClass = pluginLoader.load(testScraper, TestInterface.class);
        assertNotNull(loadedClass);
        assertTrue(loadedClass.successfulLoaded());
    }

    @Test
    void loadTwice() throws PluginLoader.LoadingException {
        InputStream stream = ClassLoader.getSystemResourceAsStream("pluginloader/Plugin.java");
        assert stream != null;
        String testScraper = new BufferedReader(new InputStreamReader(stream)).lines().collect(Collectors.joining(System.lineSeparator()));
        PluginLoader pluginLoader = new PluginLoader();
        TestInterface loadedClass = pluginLoader.load(testScraper, TestInterface.class);
        assertNotNull(loadedClass);
        assertTrue(loadedClass.successfulLoaded());

        stream = ClassLoader.getSystemResourceAsStream("pluginloader/Plugin.java");
        assert stream != null;
        testScraper = new BufferedReader(new InputStreamReader(stream)).lines().collect(Collectors.joining(System.lineSeparator()));
        pluginLoader = new PluginLoader();
        loadedClass = pluginLoader.load(testScraper, TestInterface.class);
        assertNotNull(loadedClass);
        assertTrue(loadedClass.successfulLoaded());
    }

    @Test
    void liveUpdateSameLoaderInstance() throws PluginLoader.LoadingException {
        PluginLoader pluginLoader = new PluginLoader();

        String originalPluginCode =
                "package pluginloader;\n" +
                "import pluginloader.PluginLoaderTest;\n" +
                "\n" +
                "public class Plugin implements PluginLoaderTest.LiveUpdateTestInterface {\n" +
                "    @Override\n" +
                "    public boolean updated(){\n" +
                "        return false;\n" +
                "    }\n" +
                "}";
        LiveUpdateTestInterface originalPlugin = pluginLoader.load(originalPluginCode, LiveUpdateTestInterface.class);
        assertNotNull(originalPlugin);
        assertFalse(originalPlugin.updated());

        String updatedPluginCode =
                "package pluginloader;\n" +
                "import pluginloader.PluginLoaderTest;\n" +
                "\n" +
                "public class Plugin implements PluginLoaderTest.LiveUpdateTestInterface {\n" +
                "    @Override\n" +
                "    public boolean updated(){\n" +
                "        return true;\n" +
                "    }\n" +
                "}";
        LiveUpdateTestInterface updatedPlugin = pluginLoader.load(updatedPluginCode, LiveUpdateTestInterface.class);
        assertNotNull(updatedPlugin);
        assertTrue(updatedPlugin.updated());
    }

    @Test
    void liveUpdateDifferentLoaderInstances() throws PluginLoader.LoadingException {
        String originalPluginCode =
                "package pluginloader;\n" +
                "import pluginloader.PluginLoaderTest;\n" +
                "\n" +
                "public class Plugin implements PluginLoaderTest.LiveUpdateTestInterface {\n" +
                "    @Override\n" +
                "    public boolean updated(){\n" +
                "        return false;\n" +
                "    }\n" +
                "}";
        LiveUpdateTestInterface originalPlugin = new PluginLoader().load(originalPluginCode, LiveUpdateTestInterface.class);
        assertNotNull(originalPlugin);
        assertFalse(originalPlugin.updated());

        String updatedPluginCode =
                "package pluginloader;\n" +
                "import pluginloader.PluginLoaderTest;\n" +
                "\n" +
                "public class Plugin implements PluginLoaderTest.LiveUpdateTestInterface {\n" +
                "    @Override\n" +
                "    public boolean updated(){\n" +
                "        return true;\n" +
                "    }\n" +
                "}";
        LiveUpdateTestInterface updatedPlugin = new PluginLoader().load(updatedPluginCode, LiveUpdateTestInterface.class);
        assertNotNull(updatedPlugin);
        assertTrue(updatedPlugin.updated());
    }

    @Test
    void loadPluginWithGenerics() throws PluginLoader.LoadingException {
        InputStream stream = ClassLoader.getSystemResourceAsStream("pluginloader/GenericsTestPlugin.java");
        assert stream != null;
        String testScraper = new BufferedReader(new InputStreamReader(stream)).lines().collect(Collectors.joining(System.lineSeparator()));
        PluginLoader pluginLoader = new PluginLoader();
        TestInterface loadedClass = pluginLoader.load(testScraper, TestInterface.class);
        assertNotNull(loadedClass);
        assertTrue(loadedClass.successfulLoaded());
    }

    @Test
    void loadInvalidPlugin() {
        InputStream stream = ClassLoader.getSystemResourceAsStream("pluginloader/InvalidPlugin.java");
        assert stream != null;
        String testScraper = new BufferedReader(new InputStreamReader(stream)).lines().collect(Collectors.joining(System.lineSeparator()));
        PluginLoader pluginLoader = new PluginLoader();
        assertThrows(PluginLoader.LoadingException.class, () -> pluginLoader.load(testScraper, TestInterface.class));
    }

    @Test
    void loadFromJarInputStream() throws IOException, PluginLoader.LoadingException {
        InputStream stream = ClassLoader.getSystemResourceAsStream("pluginloader/plugin.lang-und-schwarz-collector-plugin-1.0.jar");
        assert stream != null;
        JarInputStream jarInputStream = new JarInputStream(stream);
        PluginLoader pluginLoader = new PluginLoader();
        CollectorPlugin collectorPlugin = pluginLoader.load(jarInputStream, CollectorPlugin.class);
        assertNotNull(collectorPlugin);
        assertEquals("Lang & Schwarz Collector Plugin", collectorPlugin.getName());
    }
}